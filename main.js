const createOption = (text, value) => {
  let option = document.createElement('option')
  option.value = value
  option.text = text
  return option
}

const isHidden = el => {
  let style = window.getComputedStyle(el)
  return style.display === 'none'
}

const toggleDisplay = element => {
  element.style.display = isHidden(element) ? "block" : "none"
}

const hide = element => { element.style.display = "none" }

let columns = []
for (let c of document.querySelectorAll('.board-column')) {
  columns.push(c)
}

const updateColumnsFromStorage = () => {
  chrome.storage.sync.get('columns', data => {
    for (let columnNumber in data.columns) {
      if (data.columns[columnNumber]) { hide(columns[columnNumber]) }
    }
  })
}

updateColumnsFromStorage()

const select = document.createElement('select')
select.appendChild(createOption("Hide/Show Columns", null))
for (let i = 0; i < columns.length; i++) {
  let name = columns[i].querySelector('.board-column-name')['dataset'].displayName
  select.appendChild(createOption(name, i))
}

const insertReference = document.querySelector('.page-footer a:last-child')
insertReference.parentNode.insertBefore(select, insertReference.nextElementSibling)

select.addEventListener("change", (event) => {
  const number = parseInt(event.target.value)
  const el = columns[number]
  toggleDisplay(el)
  select.value = null

  const newValue = isHidden(el) ? true : false

  chrome.storage.sync.get('columns', data => {
    obj = data.columns || {}
    obj[number] = newValue
    chrome.storage.sync.set({ 'columns': obj })
  })
})
